#!/usr/bin/env python3

# script to automate updateing hostgroups in Stealthwatch based on the contents of a blocklist.
#
# - needs access credentials to the SMC web interface (with role ???)

# code based on examples from ciscoDevNet:
# https://github.com/CiscoDevNet/stealthwatch-enterprise-sample-scripts/blob/master/python/get_tags.py


import requests
import json
from requests.auth import HTTPBasicAuth  # or HTTPDigestAuth, or OAuth1, etc.
from requests import Session
import logging
from math import ceil

smc_base_url = None
smc_domain_id = None
smc_username = None
smc_password = None

########################################################################
## REST API
rest_auth_url = '/token/v2/authenticate'
XSRF_HEADER_NAME = 'X-XSRF-TOKEN'


def rest_init(username,password):
    session = requests.Session()
    response = session.post(smc_base_url+'/token/v2/authenticate',
                            data = {'username':username,
                                    'password':password})
    if response.status_code == 200:
        # Set XSRF token for future requests
        for cookie in response.cookies:
            if cookie.name == 'XSRF-TOKEN':
                session.headers.update({XSRF_HEADER_NAME: cookie.value})
                break
        return session
    else:
        return None

def rest_list_tenants(session):
    resp = session.get(smc_base_url + '/sw-reporting/v1/tenants/')
    return json.loads(resp.content)['data']

def rest_list_hostgroups(session,domain_id):
    resp = session.get(smc_base_url+'/smc-configuration/rest/v1/tenants/' + str(domain_id) + '/tags/')
    return json.loads(resp.content)['data']

def rest_find_hostgroup(session, domain_id, name=None, exact_match=False):
    """search hostgroups with matching name or id"""
    groups = rest_list_hostgroups(session,domain_id)
    if not(name):
        return None
    if exact_match:
        return [g for g in groups if g['name']==name]
    else:
        return [g for g in groups
                if (g['name'].find(name)>-1)]

def rest_get_hostgroup(session,domain_id,hostgroup_id):
    resp = session.get(smc_base_url+'/smc-configuration/rest/v1/tenants/' + str(domain_id) + '/tags/' + str(hostgroup_id) + '/')
    return json.loads(resp.content)['data']

def rest_update_hostgroup(session,domain_id,hostgroup_id,json_data):
    url = smc_base_url + '/smc-configuration/rest/v1/tenants/' + str(domain_id) + '/tags/' + str(hostgroup_id)
    request_headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    response = session.request("PUT", url, headers=request_headers,
                               data=json.dumps(json_data))
    if (response.status_code == 200):
        return True
    else:
        logging.error(f"Error updating host group {hostgroup_id}, HTTP status: {response.status_code}, details: {response.content}")
    return None

def log_hostgroup_changes(hostgroup, old,new):
    old = set(old)
    new = set(new)
    added = list(new.difference(old))
    removed = list(old.difference(new))
    logging.info(f'Hostgroup {hostgroup} updated. Added {len(added)} IPs. Removed {len(removed)} IPs.')
    if added or removed:
        chunksize=20
        for i in range(0, len(added), chunksize):
            logging.info(f'Hostgroup {hostgroup} added IPs: {added[i:i+chunksize]}')
        for i in range(0, len(removed), chunksize):
            logging.info(f'Hostgroup {hostgroup} removed IPs: {removed[i:i+chunksize]}')


def sync_hostgroup_ips(session,domain_id,hostgroup,ip_ranges):
    """synchronize the given ip ranges to the hostgroup"""
    try:
        hostgroup_id = int(hostgroup)
    except ValueError:
        g = rest_find_hostgroup(session,domain_id,hostgroup,exact=True)
        if g:
            hostgroup_id = g['id']
    assert(hostgroup_id)

    group = rest_get_hostgroup(session, domain_id, hostgroup_id)
    ip_ranges = sorted(ip_ranges)
    old = sorted(group['ranges'])
    result = None
    if not(old == ip_ranges):
        group['ranges'] = ip_ranges
        result = rest_update_hostgroup(session, domain_id, hostgroup_id, group)
    log_hostgroup_changes(group['name'], old, ip_ranges)
    return result





########################################################################
## high level entry point for syncing IP lists
##

def ips_from_file(filename):
    """Read IPs from filename.
Expects 1 IP/network per line. Ignore lines containing # characters."""
    ips = []
    f = None
    try:
        f = open(filename, mode='r')
        for l in f.readlines():
            if l.find('#')==-1:
                ips.append(l.strip())
    except Exception as e:
        logging.error("Error reading IPs from file %s: %s" %(filename, e))
    finally:
        if f and not(f.closed):
            f.close()
    return ips


def sync_hostgroups(session,domain_id,hostgroups_map):
    """Update stealthwatch hostgroups with IP addresses from
hostgroups_map (a dict of hostgroup -> file-w-IPs)."""
    if not(hostgroups_map):
        logging.warn('No hostgroups configured')
    for (hostgroup,filename) in hostgroups_map.items():
        g = rest_find_hostgroup(session,domain_id,hostgroup)
        if not(g):
            logging.warning("Cannot find hostgroup %s ins stealthwatch, skipping.")
            continue
        ips = ips_from_file(filename)
        if not(ips):
            logging.warning("No IPs in file '%s', skipping." % (filename,))
        else:
            logging.info('Syncing hostgroup %s from file %s' % (hostgroup,filename))
            sync_hostgroup_ips(session, domain_id, g[0]['id'], ips)


def main(user=None,pwd=None,domain=None,hostgroup_config=None):
    global smc_username, smc_password, smc_domain_id, smc_hostgroups
    if not(user):
        user = smc_username
    if not(pwd):
        pwd = smc_password
    if not(domain):
        domain = smc_domain_id
    if not(hostgroup_config):
        hostgroup_config = smc_hostgroups

    session = rest_init(user, pwd)
    if not(session):
        logging.error("Error logging in")
    else:
        sync_hostgroups(session,domain,hostgroup_config)

if __name__ == '__main__':
    ## read config from parameters.py
    from parameters import *
    logging.basicConfig(level=loglevel)
    main()
