#!/bin/bash
set -e

# SSHD in Sandbox betreiben 
# - Schreibrechte auf / und /etc mittels systemd entziehen
# - separater SSHD mit Schreibrechten mit Zugriff nur aus Admin-Netz
# - privilegierter SSHD wird mittels NAT vor untrusted Clients versteckt
# - um lokale SSH Logins zu verhindern wird im privilegierten SSHD
#   Match Address/AllowGroups eingebaut

ADMIN_NET=192.168.21.49/32

# Admin-SSHD auf Admin-Netze beschränken
cat > /etc/ssh/sshd_config <<EOF

Port 22
ListenAddress 0.0.0.0

HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

# prevent slowdown in case of DNS issues
UseDNS no

SyslogFacility AUTHPRIV
PubkeyAuthentication yes
AuthorizedKeysFile      .ssh/authorized_keys
# for certificate authentication, this must be enabled
#AuthorizedPrincipalsFile none
PermitEmptyPasswords no
PasswordAuthentication yes
KerberosAuthentication no
GSSAPIAuthentication yes
GSSAPICleanupCredentials no
UsePAM yes
# shell users can circumvent forwarding restrictions with netcat/socat
AllowAgentForwarding yes
AllowTcpForwarding yes
X11Forwarding yes
# Accept locale-related environment variables
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS

# prevent access from outside admin network (including localhost) to 
# this ssh daemon
PermitRootLogin no
AllowGroups none

Match Address $ADMIN_NET
  PermitRootLogin without-password
  # root is also subject to AllowGroups restrictions, 
  # don't forget root group here!
  AllowGroups root wheel

EOF


cat > /etc/systemd/system/sshd_unprivileged.service <<EOF
[Unit]
Description=OpenSSH server daemon for unprivileged users
Documentation=man:sshd(8) man:sshd_config(5)
After=network.target sshd-keygen.service
Wants=sshd-keygen.service

[Service]
EnvironmentFile=/etc/sysconfig/sshd
EnvironmentFile=/etc/sysconfig/sshd_unprivileged
ExecStart=/usr/sbin/sshd -D \$OPTIONS
ExecReload=/bin/kill -HUP \$MAINPID
KillMode=process
Restart=on-failure
RestartSec=42s

# prevent becoming root after dropping privileges. breaks unix users, ldap works.
NoNewPrivileges=true

# remount /usr, /boot, /boot/efi, /etc readonly
ProtectSystem=full

# remove capabilities from processes, the following are required for ssh to function
CapabilityBoundingSet=CAP_SYS_CHROOT CAP_AUDIT_WRITE CAP_SETUID CAP_SETGID CAP_DAC_READ_SEARCH CAP_CHOWN

# disable syscall categoriess
SystemCallFilter=~@raw-io @module @mount @swap @obsolete

# Useless/Questionable settings
# 
# prevent accessing/creating devices - this breaks pty allocation and
# affects only sshd, but not user sessions. user sessions must be
# configured in user-<UID>.slice
#PrivateDevices=yes
# this will assign a context to sshd (not the logged in users)
#SELinuxContext=
# separate /tmp for all ssh sessions
#PrivateTmp=true
# resource limits will apply only to sshd, and also user sessions
#LimitNOFILE=1024
#LimitDATA=4G

[Install]
WantedBy=multi-user.target
EOF

cat > /etc/sysconfig/sshd_unprivileged <<EOF
OPTIONS="-f /etc/ssh/sshd_unprivileged_config"
EOF

cat > /etc/ssh/sshd_unprivileged_config <<EOF
# config of unprivileged sshd

# port needs to be whitelisted in selinux
Port 2222
ListenAddress 0.0.0.0

# this is for unprivileged users only
PermitRootLogin no

HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

# prevent slowdown in case of DNS issues
UseDNS no

SyslogFacility AUTHPRIV
PubkeyAuthentication yes
AuthorizedKeysFile      .ssh/authorized_keys
# for certificate authentication, this must be enabled
#AuthorizedPrincipalsFile none
PermitEmptyPasswords no
PasswordAuthentication yes
KerberosAuthentication no
GSSAPIAuthentication yes
GSSAPICleanupCredentials no
UsePAM yes
# shell users can circumvent forwarding restrictions with netcat/socat
AllowAgentForwarding yes
AllowTcpForwarding yes
X11Forwarding yes
# Accept locale-related environment variables
AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
AcceptEnv XMODIFIERS

EOF

# assign port 2222 to sshd in SELinux
semanage port -a -t ssh_port_t -p tcp 2222


# open ports in firewall
# standard ssh (privileged)
firewall-cmd --add-service=ssh
# unprivileged sshd
firewall-cmd --add-port=2222/tcp

# port forwarding of untrusted ssh connections to port 2222
firewall-cmd --direct --add-rule ipv4 nat PREROUTING 1 ! -s $ADMIN_NET -p tcp -m tcp --dport 22 -j DNAT --to-destination :2222

# persist firewall config
firewall-cmd --runtime-to-permanent

# enable and start sshds
systemctl daemon-reload
systemctl enable sshd
systemctl enable sshd_unprivileged
systemctl restart sshd
systemctl restart sshd_unprivileged
