#!/usr/bin/env python3

__doc__ = '''
Block all outgoing network traffic except allow-listed traffic.
This was created to deal with changing IP addresses of hosts
which standard iptables cannot handle.

Edit the list of allowed host at the top of this script. Then run this
script periodically. E.g. add a line to /etc/crontab:
*/5 * * * * root /usr/local/sbin/block-outgoing-traffic.py

Requirements: python3, iptables, ipset
in Debian/Ubuntu: sudo apt install python3 iptables ipset

'''

# DNS, NTP, Update Servers
# add more hosts if required
allowed_hosts = ( 'dnsresolver1.zih.tu-dresden.de',
                  'dnsresolver2.zih.tu-dresden.de',
                  'ntp1.zih.tu-dresden.de',
                  'ntp2.zih.tu-dresden.de',
                  'de.archive.ubuntu.com',
                  'security.ubuntu.com',
                  '127.0.0.1',
                  '127.0.0.53' # local resolver on systemd-based systems
                 )

import os
from socket import gethostbyname_ex
from subprocess import check_output
import sys


setname = 'ALLOWED_OUTGOING'
marker = 'LIMIT OUTGOING'
ipset = '/usr/sbin/ipset'
iptables = '/usr/sbin/iptables'

def hosts_to_ips(hosts):
    "lookup hostnames and add resulting IPs to `ips`"
    ips = set()    
    for host in hosts:
        res = gethostbyname_ex(host)
        if res:
            ips = ips.union(res[2])
    return ips

def check_lines(cmd):
    return check_output(cmd, text=1, encoding = 'utf-8').splitlines()

def list_ipsets():
    sets = []
    for l in check_lines([ipset, 'list']):
        if l.startswith('Name:'):
            sets.append(l.split(':')[1].strip())
    return sets

def setup_ipsets():
    sets = list_ipsets()
    if not(setname in sets and setname+'_TMP' in sets):
        check_output([ipset, 'create', setname, 'hash:net'])
        check_output([ipset, 'create', setname+'_TMP', 'hash:net'])

def format_iptables_rules(chain='OUTPUT'):
    return (f'-A {chain} -m set --match-set {setname} dst -m comment --comment "{marker} Accept" -j ACCEPT',
        f'-A {chain} -m comment --comment "{marker} Reject" -j REJECT')

def setup_iptables_rules(chain='OUTPUT'):
    rule_exists = False
    for line in check_lines([iptables, '-S', chain]):
        if line.find(f'--comment "{marker}')>-1:
            rule_exists = True
    if not(rule_exists):
        for rule in format_iptables_rules(chain):
            os.system(f'{iptables} {rule}')

   
def update_ipset(ips):
    os.system(f'{ipset} flush {setname}_TMP')
    for ip in ips:
        os.system(f'{ipset} add {setname}_TMP {ip}')
    os.system(f'{ipset} swap {setname}_TMP {setname}')

def check_ufw_active():
    for line in check_lines([iptables, '-S', 'OUTPUT']):
        if line.find(f'ufw-track-output')>-1:
            return True


def main():
    setup_ipsets()
    ips = hosts_to_ips(allowed_hosts)
    update_ipset(ips)
    # If ufw is active, add rules to different chain. Otherwise the
    # rule won't work.
    if check_ufw_active():
        chain = 'ufw-after-output'
    else:
        chain = 'OUTPUT'
    # add iptables rules after ipsets have been set up.
    # otherwise, resolving hostnames might not be possible any more.
    setup_iptables_rules(chain)

def usage():
    print(f"Usage: {sys.argv[0]}")
    print(__doc__)
    

if __name__ == '__main__':
    if len(sys.argv)>1 and sys.argv[1] in ('-h'):
        usage()
        sys.exit()
    from parameters import *
    main()
