#!/usr/bin/env python3

__doc__ = '''Fetch log4j IoC from Threatfox and store IPs (not domain or urls) in a file'''

import requests
import json
import logging
import socket

api_url = 'https://threatfox-api.abuse.ch/api/v1/'

def download_log4j_ips(output_file):
    req = { 'query': 'taginfo',
            'tag'  : 'log4j',
            'limit': -1
           }
    resp = requests.post(api_url, json=req)
    if resp.status_code == 200:
        data = json.loads(resp.content)

        if data['query_status'] == 'ok':
            ips = set()
            for ioc in data['data']:
                # ignore anything that's not an IP or IP:Port
                if ioc['ioc_type'] in ('ip', 'ip:port'):
                    raw = ioc['ioc']
                    if raw.find('.')>=0:
                        # it contains an ip
                        ip = raw.split(':')[0]
                        ips.add(ip)
                    else:
                        # TODO might be IPv6?
                        pass
            with open(output_file, 'w') as out:
                logging.info(f'Writing {len(ips)} log4j IoCs to file {output_file}')
                print(*ips, sep='\n', file=out)
        else:
            logging.error(f"Error downloading Threatfox data {data['query_status']}")
            return 0
    else:
        logging.error(f"Error downloading Threatfox data {resp.status_code}: {resp.content}")
        return 0




if __name__ == '__main__':
    from parameters import *
    logging.basicConfig(level=loglevel)
    if log4j_iocs_file:
        download_log4j_ips(log4j_iocs_file)
    else:
        logging.warn("No downloads specified")
